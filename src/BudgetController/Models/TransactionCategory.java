package BudgetController.Models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "TransactionsCategories")
public class TransactionCategory {

    public static final String ID_FIELD_NAME = "id";
    @DatabaseField(generatedId = true, columnName = ID_FIELD_NAME)
    private int id;

    public static final String NAME_FIELD_NAME = "name";
    @DatabaseField(canBeNull = false, columnName = NAME_FIELD_NAME)
    private String name;

    public static final String IS_INCOME_FIELD_NAME = "isIncome";
    @DatabaseField(canBeNull = false, columnName = IS_INCOME_FIELD_NAME)
    private boolean isIncome;

    public TransactionCategory() {
    }

    public TransactionCategory(String name, boolean isIncome) {
        this.name = name;
        this.isIncome = isIncome;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIncome() {
        return isIncome;
    }

    public void setIncome(boolean income) {
        isIncome = income;
    }

    public int getId() { return id; }
}
