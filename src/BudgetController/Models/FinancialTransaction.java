package BudgetController.Models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.math.BigDecimal;
import java.util.Date;

@DatabaseTable(tableName = "FinancialTransactions")
public class FinancialTransaction {

    public static final String ID_FIELD_NAME = "id";
    @DatabaseField(generatedId = true, columnName = ID_FIELD_NAME)
    private int id;

    public static final String AMOUNT_FIELD_NAME = "amount";
    @DatabaseField(canBeNull = false, columnName = AMOUNT_FIELD_NAME, dataType = DataType.BIG_DECIMAL_NUMERIC)
    private BigDecimal amount;

    public static final String DATE_FIELD_NAME = "date";
    @DatabaseField(canBeNull = false, columnName = DATE_FIELD_NAME, dataType = DataType.DATE_LONG)
    private Date date;

    public static final String CATEGORY_FIELD_NAME = "category";
    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true, canBeNull = false, columnName = CATEGORY_FIELD_NAME)
    private TransactionCategory category;

    public static final String DESCRIPTION_FIELD_NAME = "description";
    @DatabaseField(canBeNull = false, columnName = DESCRIPTION_FIELD_NAME)
    private String description;

    public FinancialTransaction() {
    }

    public FinancialTransaction(BigDecimal amount, Date date, TransactionCategory category, String description) {
        this.amount = amount;
        this.date = date;
        this.category = category;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Date getDate() {
        return date;
    }

    public TransactionCategory getCategory() {
        return category;
    }

    public String getDescription() {
        return description;
    }
}
