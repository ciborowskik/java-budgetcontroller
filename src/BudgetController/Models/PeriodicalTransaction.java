package BudgetController.Models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.math.BigDecimal;
import java.util.Date;

@DatabaseTable(tableName = "PeriodicalTransactions")
public class PeriodicalTransaction {

    public static final String ID_FIELD_NAME = "id";
    @DatabaseField(generatedId = true, columnName = ID_FIELD_NAME)
    private int id;

    public static final String CATEGORY_FIELD_NAME = "category";
    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true, canBeNull = false, columnName = CATEGORY_FIELD_NAME)
    private TransactionCategory category;

    public static final String DESCRIPTION_FIELD_NAME = "description";
    @DatabaseField(columnName = DESCRIPTION_FIELD_NAME)
    private String description;

    public static final String DEFAULT_VALUE_FIELD_NAME = "defaultValue";
    @DatabaseField(canBeNull = false, columnName = DEFAULT_VALUE_FIELD_NAME, dataType = DataType.BIG_DECIMAL_NUMERIC)
    private BigDecimal defaultValue;

    public static final String DATE_FIELD_NAME = "date";
    @DatabaseField(canBeNull = false, columnName = DATE_FIELD_NAME, dataType = DataType.DATE_LONG)
    private Date date;

    public static final String IS_ACTIVE_FIELD_NAME = "isActive";
    @DatabaseField(canBeNull = false, columnName = IS_ACTIVE_FIELD_NAME)
    private boolean isActive;

    public PeriodicalTransaction() {
    }

    public PeriodicalTransaction(TransactionCategory category, String description, BigDecimal defaultValue, Date date, boolean isActive) {
        this.category = category;
        this.description = description;
        this.defaultValue = defaultValue;
        this.date = date;
        this.isActive = isActive;
    }

    public TransactionCategory getCategory() {
        return category;
    }

    public void setCategory(TransactionCategory category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getDefaultValue() {return defaultValue; }

    public void setDefaultValue(BigDecimal defaultValue) { this.defaultValue = defaultValue; }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public int getId() { return id; }
}