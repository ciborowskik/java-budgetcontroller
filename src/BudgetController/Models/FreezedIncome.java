package BudgetController.Models;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.math.BigDecimal;
import java.util.Date;

@DatabaseTable(tableName = "FreezedIncomes")
public class FreezedIncome {

    public static final String ID_FIELD_NAME = "id";
    @DatabaseField(generatedId = true, columnName = ID_FIELD_NAME)
    private int id;

    public static final String AMOUNT_FIELD_NAME = "amount";
    @DatabaseField(canBeNull = false, columnName = AMOUNT_FIELD_NAME, dataType = DataType.BIG_DECIMAL_NUMERIC)
    private BigDecimal amount;

    public static final String START_DATE_FIELD_NAME = "startDate";
    @DatabaseField(canBeNull = false, columnName = START_DATE_FIELD_NAME, dataType = DataType.DATE_LONG)
    private Date startDate;

    public static final String END_DATE_FIELD_NAME = "endDate";
    @DatabaseField(canBeNull = true, columnName = END_DATE_FIELD_NAME, dataType = DataType.DATE_LONG)
    private Date endDate;

    public static final String DESCRIPTION_FIELD_NAME = "description";
    @DatabaseField(canBeNull = false, columnName = DESCRIPTION_FIELD_NAME)
    private String description;

    public static final String CATEGORY_FIELD_NAME = "category";
    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true, canBeNull = false, columnName = CATEGORY_FIELD_NAME)
    private TransactionCategory category;

    public int getId() {
        return id;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public String getDescription() {
        return description;
    }

    public TransactionCategory getCategory() {
        return category;
    }

    public FreezedIncome() {
    }

    public FreezedIncome(BigDecimal amount, Date startDate, Date endDate, String description, TransactionCategory category) {
        this.amount = amount;
        this.startDate = startDate;
        this.endDate = endDate;
        this.description = description;
        this.category = category;
    }
}
