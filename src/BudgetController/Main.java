package BudgetController;

import BudgetController.Models.*;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ResourceBundle;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        ResourceBundle bundle = ResourceBundle.getBundle("BudgetController/Strings");

        String databaseUrl = bundle.getString("databaseFileName");
        Path path = Paths.get(databaseUrl);
        if(!Files.exists(path)){
            createDatabase("jdbc:sqlite:" + databaseUrl, bundle);
        }

        Parent root = FXMLLoader.load(getClass().getResource("/BudgetController/Views/HomePageView.fxml"), bundle);
        primaryStage.setTitle(bundle.getString("appName"));
        primaryStage.getIcons().add(new Image(this.getClass().getResource("icon.png").toString()));
        Scene scene = new Scene(root);
        primaryStage.setMinWidth(1000);
        primaryStage.setMinHeight(750);
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    public static void main(String[] args)
    {
        launch(args);
    }

    private static void createDatabase(String databaseUrl, ResourceBundle bundle)
    {

        try {
            ConnectionSource connectionSource = new JdbcConnectionSource(databaseUrl);

            TableUtils.createTable(connectionSource, FinancialTransaction.class);
            TableUtils.createTable(connectionSource, TransactionCategory.class);
            TableUtils.createTable(connectionSource, PeriodicalTransaction.class);
            TableUtils.createTable(connectionSource, FreezedIncome.class);

            Dao<TransactionCategory, String> dao = DaoManager.createDao(connectionSource, TransactionCategory.class);
            dao.create(new TransactionCategory(bundle.getString("phone"), false));
            dao.create(new TransactionCategory(bundle.getString("rent"), false));
            dao.create(new TransactionCategory(bundle.getString("salary"), true));
            dao.create(new TransactionCategory(bundle.getString("clothes"), false));
            dao.create(new TransactionCategory(bundle.getString("electrityBill"), false));
            dao.create(new TransactionCategory(bundle.getString("food"), false));
            dao.create(new TransactionCategory(bundle.getString("gasoline"), false));

            connectionSource.close();

        } catch (SQLException e) {
            System.out.println("Problem with creating tables in DB");
        } catch (IOException e) {
            System.out.println("Problem with closing ConnectionSource");
        }
    }
}
