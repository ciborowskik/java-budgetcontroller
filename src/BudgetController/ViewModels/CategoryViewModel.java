package BudgetController.ViewModels;

import BudgetController.Exceptions.CannotDeleteCategoryInUseException;
import BudgetController.Exceptions.ItemAlreadyExistsException;
import BudgetController.Models.FinancialTransaction;
import BudgetController.Models.FreezedIncome;
import BudgetController.Models.PeriodicalTransaction;
import BudgetController.Models.TransactionCategory;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CategoryViewModel extends BaseViewModel {
    private Dao<TransactionCategory, String> dao;

    public CategoryViewModel() {
        try {
            dao = DaoManager.createDao(connectionSource, TransactionCategory.class);
        }
        catch (SQLException e)
        {System.out.println("There was a problem in connecting to DB in " + this.getClass().getName());}
    }

    public TransactionCategory getCategoryById(int id)
    {
        try {
            return dao.queryForId(Integer.toString(id));
        } catch (SQLException e) {
            System.out.println("There was a problem in connecting to and reading categories from DB in " + this.getClass().getName());
        }

        return null;
    }

    public TransactionCategory getCategoryByProperties(String name, boolean isIncome)
    {
        try {
            List<TransactionCategory> categories =
                    dao.queryBuilder()
                            .where()
                            .eq(TransactionCategory.NAME_FIELD_NAME, name)
                            .and()
                            .eq(TransactionCategory.IS_INCOME_FIELD_NAME, isIncome)
                            .query();
            return categories.get(0);
        } catch (SQLException e) {
            System.out.println("There was a problem in connecting to and adding category to DB in " + this.getClass().getName());
        }

        return null;
    }

    public List<TransactionCategory> getAll() {
        try {
            return dao.queryForAll();
        } catch (SQLException e) {
            System.out.println("There was a problem in connecting to and reading categories from DB in " + this.getClass().getName());
        }

        return new ArrayList<>();
    }

    public List<TransactionCategory> getByType(Boolean isIncome) {
        try {
            return dao.queryBuilder()
                    .where()
                    .eq(TransactionCategory.IS_INCOME_FIELD_NAME, isIncome)
                    .query();
        } catch (SQLException e) {
            System.out.println("There was a problem in connecting to and reading categories from DB in " + this.getClass().getName());
        }

        return new ArrayList<>();
    }

    public void delete(int id) throws CannotDeleteCategoryInUseException
    {

        try {
            Dao allDao = DaoManager.createDao(connectionSource, FinancialTransaction.class);
            Dao periodicalDao = DaoManager.createDao(connectionSource, PeriodicalTransaction.class);
            Dao freezedDao = DaoManager.createDao(connectionSource, FreezedIncome.class);

            QueryBuilder qb = dao.queryBuilder();
            qb.where().idEq(id);

            Long allCount = allDao.queryBuilder().join(qb).countOf();
            Long periodicalCount = periodicalDao.queryBuilder().join(qb).countOf();
            Long freezedCount = freezedDao.queryBuilder().join(qb).countOf();

            if(allCount == 0 && periodicalCount == 0 && freezedCount == 0)
            {
                dao.deleteById(Integer.toString(id));
            }
            else
            {
                throw new CannotDeleteCategoryInUseException();
            }
        } catch (SQLException e) {
            System.out.println("There was a problem in connecting to and deleting category from DB in " + this.getClass().getName());
        }
    }
    public void add(TransactionCategory category) throws ItemAlreadyExistsException
    {
        if(category == null)
        {
            return;
        }
        try {
            List<TransactionCategory> existing =
                    dao.queryBuilder()
                            .where()
                            .eq(TransactionCategory.NAME_FIELD_NAME, new SelectArg(category.getName()))
                            .and()
                            .eq(TransactionCategory.IS_INCOME_FIELD_NAME, category.isIncome())
                            .query();
            if(!existing.isEmpty())
            {
                //that category already exists
                throw new ItemAlreadyExistsException();
            }
            dao.create(category);
        }
        catch(SQLException e)
        {
            System.out.println("There was a problem in connecting to and adding category to DB in " + this.getClass().getName());
        }
    }
}
