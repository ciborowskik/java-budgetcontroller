package BudgetController.ViewModels;

import BudgetController.Models.FinancialTransaction;
import BudgetController.Models.TransactionCategory;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.GenericRawResults;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.stmt.Where;
import javafx.util.Pair;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FinancialTransactionViewModel extends BaseViewModel{

    private Dao<FinancialTransaction, Integer> dao;
    private Dao<TransactionCategory, Integer> categoriesDao;

    public FinancialTransactionViewModel() {
        try {
            dao = DaoManager.createDao(connectionSource, FinancialTransaction.class);
            categoriesDao = DaoManager.createDao(connectionSource, TransactionCategory.class);
        }
        catch (SQLException e)
        {System.out.println("There was a problem in connecting to DB in " + this.getClass().getName());}
    }

    public List<FinancialTransaction> getAllTransactions() {
        try {
            return dao.queryForAll();
        } catch (SQLException e) {
            System.out.println("There was a problem in connecting to and reading transactions DB in " + this.getClass().getName());
        }

        return new ArrayList<>();
    }

    public List<FinancialTransaction> getFilteredTransactions(Date from, Date dateTo, BigDecimal amountFrom, BigDecimal amountTo, String searchString, Boolean isCategoryIncome, Integer categoryId) {

        boolean transactionsFiltersAppeared = false;
        boolean categoriesFiltersAppeared = false;

        QueryBuilder<FinancialTransaction, Integer> transactionsQB = dao.queryBuilder();
        Where<FinancialTransaction, Integer> transactionsWhere = transactionsQB.where();

        QueryBuilder<TransactionCategory, Integer> categoriesQB = categoriesDao.queryBuilder();
        Where<TransactionCategory, Integer> categoriesWhere = categoriesQB.where();

        try {
            if(from != null) {
                transactionsWhere.ge(FinancialTransaction.DATE_FIELD_NAME, from);
                transactionsFiltersAppeared = true;
            }

            if (dateTo != null) {
                if(transactionsFiltersAppeared) transactionsWhere.and();
                transactionsWhere.le(FinancialTransaction.DATE_FIELD_NAME, dateTo);
                transactionsFiltersAppeared = true;
            }

            if (amountFrom != null) {
                if(transactionsFiltersAppeared) transactionsWhere.and();
                transactionsWhere.ge(FinancialTransaction.AMOUNT_FIELD_NAME, amountFrom);
                transactionsFiltersAppeared = true;
            }

            if (amountTo != null) {
                if(transactionsFiltersAppeared) transactionsWhere.and();
                transactionsWhere.le(FinancialTransaction.AMOUNT_FIELD_NAME, amountTo);
                transactionsFiltersAppeared = true;
            }

            if (searchString != null) {
                if(transactionsFiltersAppeared) transactionsWhere.and();
                transactionsWhere.like(FinancialTransaction.DESCRIPTION_FIELD_NAME, new SelectArg("%"+searchString+"%"));
                transactionsFiltersAppeared = true;
            }

            if(isCategoryIncome != null) {
                categoriesWhere.eq(TransactionCategory.IS_INCOME_FIELD_NAME, isCategoryIncome.booleanValue());
                categoriesFiltersAppeared = true;
            }

            if (categoryId != null) {
                if(categoriesFiltersAppeared) categoriesWhere.and();
                categoriesWhere.eq(TransactionCategory.ID_FIELD_NAME, categoryId);
                categoriesFiltersAppeared = true;
            }

            if(!transactionsFiltersAppeared && !categoriesFiltersAppeared)
                return dao.queryForAll();
            else if(transactionsFiltersAppeared && !categoriesFiltersAppeared)
                return transactionsQB.query();
            else if(!transactionsFiltersAppeared && categoriesFiltersAppeared)
                return dao.queryBuilder().join(categoriesQB).query();
            else
                return transactionsQB.join(categoriesQB).query();

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return new ArrayList<>();
    }

    public boolean addTransaction(FinancialTransaction f){
        try {
            dao.create(f);
            return true;
        } catch (SQLException e) {
            System.out.println("There was a problem with creating new DB element in " + this.getClass().getName());
            return false;
        }
    }

    public boolean deleteTransactionById(int id){
        try {
            dao.deleteById(id);
            return true;
        } catch (SQLException e) {
            System.out.println("There was a problem with deleting DB element in " + this.getClass().getName());
            return false;
        }
    }

    public BigDecimal allTransactionsSum() {

        try {
            return new BigDecimal(dao.queryRawValue(
                    MessageFormat.format("select sum({0}) from FinancialTransactions",
                            FinancialTransaction.AMOUNT_FIELD_NAME)));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Pair<String,Double>> outcomesForCategories(){

        List<Pair<String,Double>> results = new ArrayList<>();

        try {
            GenericRawResults<Object[]> rawResults =
                    dao.queryRaw(
                            MessageFormat.format("select c.{0}, - sum({1}) as sum from " +
                                            "(FinancialTransactions f join TransactionsCategories c on c.{2} = 0 and f.{3} = c.{4})" +
                                            " group by c.{4} order by sum desc",
                                    TransactionCategory.NAME_FIELD_NAME,
                                    FinancialTransaction.AMOUNT_FIELD_NAME,
                                    TransactionCategory.IS_INCOME_FIELD_NAME,
                                    FinancialTransaction.CATEGORY_FIELD_NAME,
                                    TransactionCategory.ID_FIELD_NAME),
                            new DataType[]{DataType.STRING, DataType.DOUBLE}
                            );

            for (Object[] resultArray : rawResults) {
                results.add(new Pair<>((String)resultArray[0], (Double)resultArray[1]));
            }

            rawResults.close();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return results;
    }
}
