package BudgetController.ViewModels;

import BudgetController.Models.FreezedIncome;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.UpdateBuilder;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FreezedIncomeViewModel extends BaseViewModel{

    private Dao<FreezedIncome, Integer> dao;

    public FreezedIncomeViewModel() {
        try {
            dao = DaoManager.createDao(connectionSource, FreezedIncome.class);
        }
        catch (SQLException e)
        {System.out.println("There was a problem in connecting to DB in " + this.getClass().getName());}
    }

    public List<FreezedIncome> getAllFreezedIncomes() {
        try {
            return dao.queryForAll();
        } catch (SQLException e) {
            System.out.println("There was a problem in connecting to and reading transactions DB in " + this.getClass().getName());
        }

        return new ArrayList<>();
    }

    public List<FreezedIncome> getActiveFreezedIncomes() {
        try {
            return dao.queryBuilder()
                    .where()
                    .isNull(FreezedIncome.END_DATE_FIELD_NAME)
                    .query();
        } catch (SQLException e) {
            System.out.println("There was a problem in connecting to and reading transactions DB in " + this.getClass().getName());
        }

        return new ArrayList<>();
    }

    public List<FreezedIncome> getFinalizedFreezedIncomes() {
        try {
            return dao.queryBuilder()
                    .where()
                    .isNotNull(FreezedIncome.END_DATE_FIELD_NAME)
                    .query();
        } catch (SQLException e) {
            System.out.println("There was a problem in connecting to and reading transactions DB in " + this.getClass().getName());
        }

        return new ArrayList<>();
    }

    public FreezedIncome getById(int id) {
        try {
            return dao.queryForId(id);
        } catch (SQLException e) {
            System.out.println("There was a problem in connecting to and reading transactions DB in " + this.getClass().getName());
        }

        return null;
    }

    public boolean addFreezedIncome(FreezedIncome f){
        try {
            dao.create(f);
            return true;
        } catch (SQLException e) {
            //System.out.println("There was a problem with creating new DB element in " + this.getClass().getName());
            e.printStackTrace();
            return false;
        }
    }

    public boolean addToAmount(int id, BigDecimal addition){
        try {
            BigDecimal newAmount = dao.queryForId(id).getAmount().add(addition);
            UpdateBuilder<FreezedIncome, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.updateColumnValue(FreezedIncome.AMOUNT_FIELD_NAME, newAmount);
            updateBuilder.where().eq(FreezedIncome.ID_FIELD_NAME, id);
            updateBuilder.update();

            return true;
        } catch (SQLException e) {
            System.out.println("There was a problem with creating new DB element in " + this.getClass().getName());
            return false;
        }
    }



    public boolean finalizeFreezedIncome(int id, Date date){
        try {
            UpdateBuilder<FreezedIncome, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.updateColumnValue(FreezedIncome.END_DATE_FIELD_NAME, date);
            updateBuilder.where().eq(FreezedIncome.ID_FIELD_NAME, id);
            updateBuilder.update();

            return true;
        } catch (SQLException e) {
            System.out.println("There was a problem with creating new DB element in " + this.getClass().getName());
            return false;
        }
    }

}
