package BudgetController.ViewModels;

import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

import java.sql.SQLException;
import java.util.ResourceBundle;

public class BaseViewModel {
    protected ConnectionSource connectionSource;

    public BaseViewModel() {

        try {
            connectionSource = new JdbcConnectionSource("jdbc:sqlite:" + ResourceBundle.getBundle("BudgetController/Strings").getString("databaseFileName"));
        }
        catch (SQLException e)
        {System.out.println("There was a problem in connecting to DB in " + this.getClass().getName());}
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        if(connectionSource != null)
        {
            connectionSource.close();
        }
    }
}
