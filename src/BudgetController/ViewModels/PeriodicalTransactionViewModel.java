package BudgetController.ViewModels;

import BudgetController.Exceptions.ItemAlreadyExistsException;
import BudgetController.Models.PeriodicalTransaction;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.stmt.SelectArg;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class PeriodicalTransactionViewModel extends BaseViewModel {
    private Dao<PeriodicalTransaction, String> dao;

    public PeriodicalTransactionViewModel() {
        try {
            dao = DaoManager.createDao(connectionSource, PeriodicalTransaction.class);
        }
        catch (SQLException e)
        {System.out.println("There was a problem in connecting to DB in " + this.getClass().getName());}
    }

    public List<PeriodicalTransaction> getAll()
    {
        try {
            return dao.queryForAll();
        }
        catch(SQLException e)
        {
            System.out.println("There was a problem in connecting to and reading periodical transaction from DB in " + this.getClass().getName());
        }
        return new ArrayList<>();
    }

    public List<PeriodicalTransaction> getByActive(Boolean isActive)
    {
        try {
            return dao.queryBuilder()
                    .where()
                    .eq(PeriodicalTransaction.IS_ACTIVE_FIELD_NAME, isActive)
                    .query();
        }
        catch(SQLException e)
        {
            System.out.println("There was a problem in connecting to and reading periodical transaction from DB in " + this.getClass().getName());
        }
        return new ArrayList<>();
    }

    public void add(PeriodicalTransaction periodicalTransaction) throws ItemAlreadyExistsException
    {
        if(periodicalTransaction == null)
        {
            return;
        }
        try {
            List<PeriodicalTransaction> existing =
                    dao.queryBuilder()
                            .where()
                            .eq(PeriodicalTransaction.CATEGORY_FIELD_NAME, periodicalTransaction.getCategory())
                            .and()
                            .eq(PeriodicalTransaction.DESCRIPTION_FIELD_NAME, new SelectArg(periodicalTransaction.getDescription()))
                            .and()
                            .eq(PeriodicalTransaction.DATE_FIELD_NAME, periodicalTransaction.getDate())
                            .and()
                            .eq(PeriodicalTransaction.DEFAULT_VALUE_FIELD_NAME, periodicalTransaction.getDefaultValue())
                            .query();
            if(!existing.isEmpty())
            {
                //that periodical transaction already exists
                throw new ItemAlreadyExistsException();
            }
            dao.create(periodicalTransaction);
        }
        catch(SQLException e)
        {
            System.out.println("There was a problem in connecting to and adding periodical transaction to DB in " + this.getClass().getName());
        }
    }

    public void delete(int id)
    {
        try {
            dao.deleteById(Integer.toString(id));
        }
        catch(SQLException e)
        {
            System.out.println("There was a problem in connecting to and deleting periodical transaction from DB in " + this.getClass().getName());
        }
    }

    public void update(PeriodicalTransaction periodicalTransaction)
    {
        try {
            dao.update(periodicalTransaction);
        }
        catch(SQLException e)
        {
            System.out.println("There was a problem in connecting to and deleting periodical transaction from DB in " + this.getClass().getName());
        }
    }


}
