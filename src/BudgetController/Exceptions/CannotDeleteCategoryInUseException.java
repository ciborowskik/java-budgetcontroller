package BudgetController.Exceptions;

public class CannotDeleteCategoryInUseException extends Exception {
    public CannotDeleteCategoryInUseException() {
    }
    public CannotDeleteCategoryInUseException(String errorMessage) {
        super(errorMessage);
    }
}
