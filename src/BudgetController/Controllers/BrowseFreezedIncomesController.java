package BudgetController.Controllers;

import BudgetController.Controllers.Validation.*;
import BudgetController.Controllers.Wrappers.FreezedIncomeWrapper;
import BudgetController.Exceptions.ItemAlreadyExistsException;
import BudgetController.Models.FinancialTransaction;
import BudgetController.Models.FreezedIncome;
import BudgetController.Models.TransactionCategory;
import BudgetController.ViewModels.CategoryViewModel;
import BudgetController.ViewModels.FinancialTransactionViewModel;
import BudgetController.ViewModels.FreezedIncomeViewModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Group;
import javafx.scene.control.*;

import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.util.Date;
import java.util.ResourceBundle;

public class BrowseFreezedIncomesController extends Controller implements Initializable {

    private Validator<String> amountValidator = new Validator<>();
    private Validator<String> amountPositiveValidator = new Validator<>();
    private Validator<LocalDate> dateValidator = new Validator<>();
    private FreezedIncomeViewModel freezedIncomeViewModel = new FreezedIncomeViewModel();
    private FinancialTransactionViewModel financialTransactionViewModel = new FinancialTransactionViewModel();
    private CategoryViewModel categoryViewModel = new CategoryViewModel();
    private ObservableList<FreezedIncomeWrapper> tableViewItems = FXCollections.observableArrayList();
    private FreezedIncomeWrapper selectedWrapper;

    @FXML private TableView<FreezedIncomeWrapper> tableView;
    @FXML private CheckBox active;
    @FXML private CheckBox finalized;

    @FXML private RadioButton increaseInvestment;
    @FXML private RadioButton takeOut;
    @FXML private RadioButton finalizeInvestment;
    @FXML private RadioButton notifyProfit;
    @FXML private DatePicker date;
    @FXML private Label dateValidationLabel;
    @FXML private TextField amount;
    @FXML private Label amountValidationLabel;
    @FXML private Label amountPositiveValidationLabel;
    @FXML private Group group;
    ResourceBundle resources;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        this.resources = resources;

        amountValidator.getValidationRules().add(
                new CannotBeEmptyStringValidationRule(resources.getString("fieldRequiredValidationMessage")),0);
        amountValidator.getValidationRules().add(
                new IsMoneyValueValidationRule(resources.getString("hasToBeMoneyValueValidationMessage")),1);
        amountValidator.validatedValueProperty().bind(amount.textProperty());
        amountValidationLabel.managedProperty().bind(amountValidator.validProperty().not());
        amountValidationLabel.textProperty().bind(amountValidator.validationMessageProperty());

        amountPositiveValidator.getValidationRules().add(
                new CannotBeEmptyStringValidationRule(resources.getString("fieldRequiredValidationMessage")),0);
        amountPositiveValidator.getValidationRules().add(
                new IsMoneyValueValidationRule(resources.getString("hasToBeMoneyValueValidationMessage")),1);
        amountPositiveValidator.getValidationRules().add(
                new IsPositiveMoneyValueValidationRule(resources.getString("hasToBePositiveValidationMessage")),2);
        amountPositiveValidator.validatedValueProperty().bind(amount.textProperty());
        amountPositiveValidationLabel.managedProperty().bind(amountPositiveValidator.validProperty().not());
        amountPositiveValidationLabel.textProperty().bind(amountPositiveValidator.validationMessageProperty());

        dateValidator.getValidationRules().add(
                new CannotBeNullValidationRule<>(resources.getString("dateHaveNotBeenSelected")),0);
        dateValidator.validatedValueProperty().bind(date.valueProperty());
        dateValidationLabel.managedProperty().bind(dateValidator.validProperty().not());
        dateValidationLabel.textProperty().bind(dateValidator.validationMessageProperty());


        group.setDisable(true);
        tableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection == null || !newSelection.isActive())
                group.setDisable(true);
            else
                group.setDisable(false);
            selectedWrapper = newSelection;
        });
        date.disableProperty().bind(notifyProfit.selectedProperty());
        amount.disableProperty().bind(finalizeInvestment.selectedProperty());


        active.setSelected(true);

        tableView.setItems(tableViewItems);
        date.setValue(LocalDate.now());

        notifyProfit.setSelected(true);

        updateTableView();
    }

    @FXML
    private void updateTableView() {
        if(active.isSelected() && finalized.isSelected())
            tableViewItems.setAll(FreezedIncomeWrapper.wrapFreezedIncomes(freezedIncomeViewModel.getAllFreezedIncomes()));
        else if(active.isSelected())
            tableViewItems.setAll(FreezedIncomeWrapper.wrapFreezedIncomes(freezedIncomeViewModel.getActiveFreezedIncomes()));
        else if(finalized.isSelected())
            tableViewItems.setAll(FreezedIncomeWrapper.wrapFreezedIncomes(freezedIncomeViewModel.getFinalizedFreezedIncomes()));
        tableView.refresh();
    }

    @FXML
    private void resetValidation(){
        amountValidator.reset();
        amountPositiveValidator.reset();
        dateValidator.reset();
    }

    @FXML
    private void activeSelectionChanged() {
        if (!active.isSelected())
            finalized.setSelected(true);
        updateTableView();
    }

    @FXML
    private void finalizedSelectionChanged() {
        if (!finalized.isSelected())
            active.setSelected(true);
        updateTableView();
    }

    private void increaseInvestmentPerform(){
        boolean v1 = amountPositiveValidator.isValid();
        boolean v2 = dateValidator.isValid();
        if(v1 && v2){
            BigDecimal amount_ = StringToMoneyConverter.convert(amount.getText());
            Date date_ = java.sql.Date.valueOf(date.getValue());

            int id = selectedWrapper.getId();
            FreezedIncome f = freezedIncomeViewModel.getById(id);

            freezedIncomeViewModel.addToAmount(id, amount_);
            financialTransactionViewModel.addTransaction(new FinancialTransaction(amount_.negate(), date_, f.getCategory(), resources.getString("invested") + " \"" + f.getDescription() + "\""));
            updateTableView();
        }
    }

    private void notifyProfitPerform(){
        if(amountValidator.isValid()){
            BigDecimal amount_ = StringToMoneyConverter.convert(amount.getText());
            int id = selectedWrapper.getId();

            freezedIncomeViewModel.addToAmount(id, amount_);
            updateTableView();
        }
    }

    private void takeOutPerform(){
        boolean v1 = amountPositiveValidator.isValid();
        boolean v2 = dateValidator.isValid();
        if(v1 && v2){
            BigDecimal amount_ = StringToMoneyConverter.convert(amount.getText());
            Date date_ = java.sql.Date.valueOf(date.getValue());

            int id = selectedWrapper.getId();
            FreezedIncome f = freezedIncomeViewModel.getById(id);

            TransactionCategory tmp = new TransactionCategory(f.getCategory().getName(), true);
            try {
                categoryViewModel.add(tmp);
            } catch (ItemAlreadyExistsException e) {}
            TransactionCategory category_ = categoryViewModel.getCategoryByProperties(f.getCategory().getName(), true);

            freezedIncomeViewModel.addToAmount(id, amount_.negate());
            financialTransactionViewModel.addTransaction(new FinancialTransaction(amount_, date_, category_, resources.getString("tookOut") + " \"" + f.getDescription() + "\""));
            updateTableView();
        }
    }

    private void finalizeInvestmentPerform(){
        if(dateValidator.isValid()){
            Date date_ = java.sql.Date.valueOf(date.getValue());

            int id = selectedWrapper.getId();
            FreezedIncome f = freezedIncomeViewModel.getById(id);

            if(f.getAmount().compareTo(BigDecimal.ZERO) > 0) {
                TransactionCategory tmp = new TransactionCategory(f.getCategory().getName(), true);
                try {
                    categoryViewModel.add(tmp);
                }
                catch (ItemAlreadyExistsException e) {}
                TransactionCategory category_ = categoryViewModel.getCategoryByProperties(f.getCategory().getName(), true);

                financialTransactionViewModel.addTransaction(new FinancialTransaction(f.getAmount(), date_, category_, resources.getString("tookOut") + " \"" + f.getDescription() + "\""));
            }
            else if(f.getAmount().compareTo(BigDecimal.ZERO) < 0){
                financialTransactionViewModel.addTransaction(new FinancialTransaction(f.getAmount(), date_, f.getCategory(), resources.getString("invested") + " \"" + f.getDescription() + "\""));
            }

            freezedIncomeViewModel.finalizeFreezedIncome(id, date_);
            freezedIncomeViewModel.addToAmount(id, f.getAmount().negate());
            updateTableView();
        }
    }

    @FXML
    private void perform(){
        if (increaseInvestment.isSelected())
            increaseInvestmentPerform();
        else if (notifyProfit.isSelected())
            notifyProfitPerform();
        else if (takeOut.isSelected())
            takeOutPerform();
        else if (finalizeInvestment.isSelected())
            finalizeInvestmentPerform();
    }
}
