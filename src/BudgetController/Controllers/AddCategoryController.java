package BudgetController.Controllers;

import BudgetController.Controllers.Validation.CannotBeEmptyStringValidationRule;
import BudgetController.Controllers.Validation.Validator;
import BudgetController.Exceptions.ItemAlreadyExistsException;
import BudgetController.Models.TransactionCategory;
import BudgetController.ViewModels.CategoryViewModel;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import java.util.ResourceBundle;


public class AddCategoryController extends Controller{

    private CategoryViewModel viewModel = new CategoryViewModel();
    private Validator<String> nameValidator = new Validator<>();

    @FXML
    private ResourceBundle resources;

    @FXML
    private TextField nameTextField;

    @FXML
    private Label nameValidationLabel;

    @FXML
    private RadioButton incomeRadioButton;
    @FXML
    private Label generalErrorsLabel;

    @FXML
    private void addCategory()
    {
        generalErrorsLabel.setText(null);
        generalErrorsLabel.setManaged(false);

        if(nameValidator.isValid())
        {
            TransactionCategory category = new TransactionCategory(nameTextField.getText(), incomeRadioButton.isSelected());
            try {
                viewModel.add(category);
            }
            catch (ItemAlreadyExistsException e)
            {
                generalErrorsLabel.setText(resources.getString("thatCategoryAlreadyExists"));
                generalErrorsLabel.setManaged(true);
                return;
            }
            navigationController.changeView("/BudgetController/Views/BrowseCategoriesView.fxml");
        }
    }

    @FXML
    private void initialize()
    {
        nameValidator.getValidationRules().add(
                new CannotBeEmptyStringValidationRule(resources.getString("fieldRequiredValidationMessage")),
                0);

        nameValidator.validatedValueProperty().bind(nameTextField.textProperty());

        nameValidationLabel.managedProperty().bind(nameValidator.validProperty().not());
        nameValidationLabel.textProperty().bind(nameValidator.validationMessageProperty());

        incomeRadioButton.setSelected(true);
    }
}
