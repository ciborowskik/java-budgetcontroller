package BudgetController.Controllers;

import BudgetController.Controllers.Validation.*;
import BudgetController.Controllers.Wrappers.CategoryWrapper;
import BudgetController.Models.FinancialTransaction;
import BudgetController.Models.TransactionCategory;
import BudgetController.ViewModels.CategoryViewModel;
import BudgetController.ViewModels.FinancialTransactionViewModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.math.BigDecimal;
import java.net.URL;
import java.time.LocalDate;
import java.util.Date;
import java.util.ResourceBundle;

public class AddTransactionController extends Controller implements Initializable {

    private Validator<String> amountValidator = new Validator<>();
    private Validator<CategoryWrapper> categoryValidator = new Validator<>();
    private Validator<LocalDate> dateValidator = new Validator<>();
    private FinancialTransactionViewModel financialTransactionViewModel = new FinancialTransactionViewModel();
    private CategoryViewModel categoryViewModel = new CategoryViewModel();
    private ObservableList<CategoryWrapper> comboBoxItems = FXCollections.observableArrayList();

    private ResourceBundle resourceBundle;

    @FXML private RadioButton income;
    @FXML private RadioButton outcome;
    @FXML private ChoiceBox<CategoryWrapper> comboBox;
    @FXML private Label categoryValidationLabel;
    @FXML private DatePicker date;
    @FXML private Label dateValidationLabel;
    @FXML private TextField amount;
    @FXML private Label amountValidationLabel;
    @FXML private TextField description;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        resourceBundle = resources;

        amountValidator.getValidationRules().add(
                new CannotBeEmptyStringValidationRule(resources.getString("fieldRequiredValidationMessage")),0);
        amountValidator.getValidationRules().add(
                new IsMoneyValueValidationRule(resources.getString("hasToBeMoneyValueValidationMessage")),1);
        amountValidator.getValidationRules().add(
                new IsPositiveMoneyValueValidationRule(resources.getString("hasToBePositiveValidationMessage")),2);
        amountValidator.validatedValueProperty().bind(amount.textProperty());
        amountValidationLabel.managedProperty().bind(amountValidator.validProperty().not());
        amountValidationLabel.textProperty().bind(amountValidator.validationMessageProperty());

        categoryValidator.getValidationRules().add(
                new CannotBeNullValidationRule<>(resources.getString("categoryHaveNotBeenSelected")),0);
        categoryValidator.validatedValueProperty().bindBidirectional(comboBox.valueProperty());
        categoryValidationLabel.managedProperty().bind(categoryValidator.validProperty().not());
        categoryValidationLabel.textProperty().bind(categoryValidator.validationMessageProperty());

        dateValidator.getValidationRules().add(
                new CannotBeNullValidationRule<>(resources.getString("dateHaveNotBeenSelected")),0);
        dateValidator.validatedValueProperty().bindBidirectional(date.valueProperty());
        dateValidationLabel.managedProperty().bind(dateValidator.validProperty().not());
        dateValidationLabel.textProperty().bind(dateValidator.validationMessageProperty());

        income.setSelected(true);
        date.setValue(LocalDate.now());

        comboBox.setItems(comboBoxItems);
        updateComboBox();
    }

    @FXML
    private void addTransaction(){

        if(isAllValid()) {
            Date date_ = java.sql.Date.valueOf(date.getValue());
            BigDecimal amount_ = StringToMoneyConverter.convert(amount.getText());
            String description_ = description.getText();
            int category_id = comboBox.getValue().getId();

            TransactionCategory category = categoryViewModel.getCategoryById(category_id);
            if(!category.isIncome())
                amount_ = amount_.negate();

            financialTransactionViewModel.addTransaction(new FinancialTransaction(amount_, date_, category, description_));
            navigationController.changeView("/BudgetController/Views/BrowseAllTransactionsView.fxml");
        }
    }

    private boolean isAllValid(){
        boolean v1 = amountValidator.isValid();
        boolean v2 = categoryValidator.isValid();
        boolean v3 = dateValidator.isValid();
        return v1 && v2  && v3;
    }

    @FXML
    private void updateComboBox(){
        if(income.isSelected())
            comboBoxItems.setAll(CategoryWrapper.wrapCategories(categoryViewModel.getByType(true), resourceBundle));
        else
            comboBoxItems.setAll(CategoryWrapper.wrapCategories(categoryViewModel.getByType(false), resourceBundle));

        if(!comboBoxItems.isEmpty())
        {
            comboBox.getSelectionModel().select(0);
        }
    }
}
