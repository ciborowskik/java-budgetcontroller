package BudgetController.Controllers;

import java.awt.Desktop;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.layout.Pane;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ResourceBundle;

public class HomePageController  implements Initializable, INavigationController {

    private ResourceBundle bundle;
    private String currentFxmlPath;

    @FXML private Pane content;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        bundle = resources;
        changeView("/BudgetController/Views/WelcomingView.fxml");
    }

    public Controller changeView(String fxmlPath){
        if(fxmlPath == currentFxmlPath)
            return null;

        content.getChildren().clear();
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource(fxmlPath));
            loader.setResources(bundle);

            content.getChildren().add(loader.load());
            currentFxmlPath = fxmlPath;

            Controller controller = loader.getController();
            if(controller == null)
            {
                return null;
            }
            controller.setNavigationController(this);
            return controller;

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @FXML
    private void goToBrowseAllTransactionsView() {
        changeView("/BudgetController/Views/BrowseAllTransactionsView.fxml");
    }

    @FXML
    private void goToAddTransactionView() {
        changeView("/BudgetController/Views/AddTransactionView.fxml");
    }

    @FXML
    private void goToAddFreezedIncomeView(){
        changeView("/BudgetController/Views/AddFreezedIncomeView.fxml");
    }

    @FXML
    private void goToBrowseFreezedIncomesView(){
        changeView("/BudgetController/Views/BrowseFreezedIncomesView.fxml");
    }

    @FXML
    private void goToAddCategoryView() {
        changeView("/BudgetController/Views/AddCategoryView.fxml");
    }

    @FXML
    private void goToWelcomingView() {
        changeView("/BudgetController/Views/WelcomingView.fxml");
    }

    @FXML
    private void goToBrowseCategoriesView() {
        changeView("/BudgetController/Views/BrowseCategoriesView.fxml");
    }

    @FXML
    private void goToBrowsePeriodicalTransactionView() {
        changeView("/BudgetController/Views/BrowsePeriodicalTransactionsView.fxml");
    }

    @FXML
    private void goToAddPeriodicalTransactionView() {
        changeView("/BudgetController/Views/AddPeriodicalTransactionView.fxml");
    }

    @FXML
    private void exit() {
        Platform.exit();
    }

    @FXML
    private void manual() {
        String pathToManual = "/BudgetController/manual.pdf";

        if(bundle.getLocale().toString().equals("pl"))
        {
            pathToManual = "/BudgetController/manual_pl.pdf";
        }

        try {
            Desktop.getDesktop().open(new File(getClass().getResource(pathToManual).toURI()));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void about() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(bundle.getString("aboutDialogTitle"));
        alert.setContentText(bundle.getString("aboutDialogText"));
        alert.setHeaderText(null);
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(getClass().getResource("/BudgetController/icon.png").toString()));
        alert.show();
    }
}
