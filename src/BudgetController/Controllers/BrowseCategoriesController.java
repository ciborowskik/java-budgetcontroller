package BudgetController.Controllers;

import BudgetController.Controllers.Wrappers.CategoryWrapper;
import BudgetController.Exceptions.CannotDeleteCategoryInUseException;
import BudgetController.ViewModels.CategoryViewModel;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class BrowseCategoriesController extends Controller {

    private CategoryViewModel viewModel = new CategoryViewModel();

    @FXML
    private ResourceBundle resources;

    @FXML
    private CheckBox showIncomeTypeCheckBox;
    @FXML
    private CheckBox showOutcomeTypeCheckBox;
    @FXML
    private Button deleteButton;

    @FXML
    private TableView tableView;

    @FXML
    private Label deleteErrorLabel;

    @FXML
    public void initialize() {
        List<CategoryWrapper> categories = CategoryWrapper.wrapCategories(viewModel.getAll(), resources);
        updateTable(categories);

        showIncomeTypeCheckBox.setSelected(true);
        showOutcomeTypeCheckBox.setSelected(true);
        deleteButton.setDisable(true);

        addDeleteButtonDisableListener();

        deleteErrorLabel.setManaged(false);
    }

    private void addDeleteButtonDisableListener()
    {
        tableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            if (newSelection == null) {
                deleteButton.setDisable(true);
            }
            else
            {
                deleteButton.setDisable(false);
            }
        });
    }

    private void updateTable(List<CategoryWrapper> categories)
    {
        if(!tableView.getItems().isEmpty()){
            tableView.getItems().clear();
        }
        ObservableList<CategoryWrapper> observableList = tableView.getItems();
        observableList.addAll(categories);
    }

    @FXML
    private void refreshTable()
    {
        Boolean showIncome = showIncomeTypeCheckBox.isSelected();
        Boolean showOutcome = showOutcomeTypeCheckBox.isSelected();

        if(!showIncome && !showOutcome)
        {
            updateTable(new ArrayList<>());
            return;
        }

        List<CategoryWrapper> categories = null;

        if(showIncome && showOutcome)
        {
            categories = CategoryWrapper.wrapCategories(viewModel.getAll(), resources);
        }
        else if(showIncome) {
            categories = CategoryWrapper.wrapCategories(viewModel.getByType(true), resources);
        }
        else if(showOutcome) {
            categories = CategoryWrapper.wrapCategories(viewModel.getByType(false), resources);
        }
        updateTable(categories);
    }

    @FXML
    private void deleteCategory()
    {
        CategoryWrapper wrapper = (CategoryWrapper)tableView.getSelectionModel().getSelectedItem();
        try {
            deleteErrorLabel.setManaged(false);
            deleteErrorLabel.setText("");
            viewModel.delete(wrapper.getId());
        }
        catch (CannotDeleteCategoryInUseException exc)
        {
            deleteErrorLabel.setManaged(true);
            deleteErrorLabel.setText(resources.getString("cannotDeleteCategoryInUse"));
        }

        refreshTable();
    }

    @FXML
    private void showIncomeSelectionChanged() {
        if (!showIncomeTypeCheckBox.isSelected())
            showOutcomeTypeCheckBox.setSelected(true);
        refreshTable();
    }

    @FXML
    private void showOutcomeSelectionChanged() {
        if (!showOutcomeTypeCheckBox.isSelected())
            showIncomeTypeCheckBox.setSelected(true);
        refreshTable();
    }

}
