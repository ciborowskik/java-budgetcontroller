package BudgetController.Controllers.Validation;

public class IsEmptyOrMoneyValueValidationRule extends CannotBeNullValidationRule<String>  {

    public IsEmptyOrMoneyValueValidationRule(String message) {
        super(message);
    }

    public IsEmptyOrMoneyValueValidationRule() {
    }

    @Override
    public Boolean isValid(String value)
    {
        if(!super.isValid(value))
            return false;
        if(value.equals(""))
            return true;

        String noWhitespaces = deleteWhitespaces(value);

        //positive or negative number with max 2 decimals after , or .
        String regex = "[-]?(([1-9][0-9]*)|([0-9]+[.,][0-9]{0,2})|(0))";

        return noWhitespaces.matches(regex);
    }

    protected String deleteWhitespaces(String s)
    {
        return s.replace(" ", "");
    }
}
