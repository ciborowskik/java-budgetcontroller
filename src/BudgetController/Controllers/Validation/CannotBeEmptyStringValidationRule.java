package BudgetController.Controllers.Validation;

public class CannotBeEmptyStringValidationRule extends CannotBeNullValidationRule<String> {
    public CannotBeEmptyStringValidationRule() {
    }
    public CannotBeEmptyStringValidationRule(String message) {
        super(message);
    }

    @Override
    public Boolean isValid(String value) {
        if(!super.isValid(value))
        {
            return false;
        }

        return !value.isEmpty();
    }
}
