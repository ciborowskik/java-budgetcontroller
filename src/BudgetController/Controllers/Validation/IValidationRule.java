package BudgetController.Controllers.Validation;


public interface IValidationRule<T> {
    Boolean isValid(T value);

    /**
     * @return message to be displayed if given value is not valid
     */
    String getValidationMessage();
}
