package BudgetController.Controllers.Validation;

public class IsLongerThanValidationRule extends CannotBeEmptyStringValidationRule {

    private int minLength;

    public IsLongerThanValidationRule(int minLength) {
        this.minLength = minLength;
    }

    public IsLongerThanValidationRule(String message, int minLength) {
        super(message);
        this.minLength = minLength;
    }

    @Override
    public Boolean isValid(String value) {
        if(!super.isValid(value))
        {
            return false;
        }

        return value.length() > minLength;
    }
}
