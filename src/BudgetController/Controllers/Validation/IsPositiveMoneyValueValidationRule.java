package BudgetController.Controllers.Validation;

public class IsPositiveMoneyValueValidationRule extends CannotBeEmptyStringValidationRule  {
    public IsPositiveMoneyValueValidationRule(String message) {
        super(message);
    }

    @Override
    public Boolean isValid(String value)
    {
        if(!super.isValid(value))
        {
            return false;
        }

        String noWhitespaces = deleteWhitespaces(value);

        //positive number with max 2 decimals after , or .
        String regex = "(([1-9][0-9]*)|([0-9]+[.,][0-9]{0,2})|(0))";

        return noWhitespaces.matches(regex);
    }

    protected String deleteWhitespaces(String s)
    {
        return s.replace(" ", "");
    }
}
