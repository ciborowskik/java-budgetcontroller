package BudgetController.Controllers.Validation;

public abstract class AbstractValidationRule<T> implements IValidationRule<T> {

    private String validationMessage = "";
    @Override
    public abstract Boolean isValid(T value);

    @Override
    public String getValidationMessage() {
        return validationMessage;
    }

    public void setValidationMessage(String message) {
        validationMessage = message;
    }
}
