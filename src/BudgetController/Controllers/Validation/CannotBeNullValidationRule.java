package BudgetController.Controllers.Validation;

public class CannotBeNullValidationRule<T> extends AbstractValidationRule<T> {

    @Override
    public Boolean isValid(T value) {
        return value != null;
    }

    public CannotBeNullValidationRule(String message) {
        setValidationMessage(message);
    }

    public CannotBeNullValidationRule() {
    }
}
