package BudgetController.Controllers.Validation;

public class IsMoneyValueValidationRule extends CannotBeEmptyStringValidationRule {

    public IsMoneyValueValidationRule(String message) {
        super(message);
    }

    public IsMoneyValueValidationRule() {
    }

    @Override
    public Boolean isValid(String value)
    {
        if(!super.isValid(value))
        {
            return false;
        }

        String noWhitespaces = deleteWhitespaces(value);
        //positive or negative number with max 2 decimals after , or .
        String regex = "[-]?(([1-9][0-9]*)|([0-9]+[.,][0-9]{0,2})|(0))";

        boolean isMoneyFormat = noWhitespaces.matches(regex);

        if(!isMoneyFormat) return false;

        return noWhitespaces.length() <= 16;
    }

    protected String deleteWhitespaces(String s)
    {
        return s.replace(" ", "");
    }

}
