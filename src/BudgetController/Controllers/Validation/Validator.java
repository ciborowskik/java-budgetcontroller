package BudgetController.Controllers.Validation;

import BudgetController.Helpers.PriorityList;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class Validator<T> {
    private SimpleStringProperty validationMessage = new SimpleStringProperty();
    private SimpleBooleanProperty valid = new SimpleBooleanProperty(true);
    private SimpleObjectProperty<T> validatedValue = new SimpleObjectProperty<>();
    private PriorityList<IValidationRule<T>> validationRules = new PriorityList<>();

    /**
     * Loops list of validation rules in order defined by priorities (from smallest to biggest).
     * Breaks loop when unfulfilled rule is found. Sets validation message taken from that rule.
     * @return true if all validation rules are fulfilled.
     */
    public Boolean validate()
    {
        for (IValidationRule<T> rule : validationRules)
        {
            if(!rule.isValid(validatedValue.getValue()))
            {
                validationMessage.set(rule.getValidationMessage());
                valid.set(false);
                return false;
            }
        }
        valid.set(true);
        validationMessage.set("");
        return true;
    }
    public void reset(){
        valid.set(true);
        validationMessage.set("");
    }

    //region Getters and Setters
    public String getValidationMessage() {
        return validationMessage.get();
    }

    public SimpleStringProperty validationMessageProperty() {
        return validationMessage;
    }

    public boolean isValid() {
        validate();
        return valid.get();
    }

    public SimpleBooleanProperty validProperty() {
        return valid;
    }

    public Object getValidatedValue() {
        return validatedValue.get();
    }

    public SimpleObjectProperty<T> validatedValueProperty() {
        return validatedValue;
    }

    public void setValidatedValue(Object validatedValue) {
        try {
            T value = (T) validatedValue;
            this.validatedValue.set(value);
        }
        catch (Exception e)
        {
            return;
        }
    }

    public PriorityList<IValidationRule<T>> getValidationRules() {
        return validationRules;
    }

    public void setValidationRules(PriorityList<IValidationRule<T>> validationRules) {
        this.validationRules = validationRules;
    }
    //endregion
}
