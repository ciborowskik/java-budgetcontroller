package BudgetController.Controllers.Validation;

import java.math.BigDecimal;

public class StringToMoneyConverter {
    public static BigDecimal convert(String s)
    {
        String noWhitespaces = s.replace(" ", "");
        String dots = noWhitespaces.replace(",", ".");
        return new BigDecimal(dots).setScale(2);
    }
}
