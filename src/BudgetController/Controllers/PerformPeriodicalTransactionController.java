package BudgetController.Controllers;

import BudgetController.Controllers.Validation.*;
import BudgetController.Helpers.DateConverter;
import BudgetController.Models.FinancialTransaction;
import BudgetController.Models.PeriodicalTransaction;
import BudgetController.Models.TransactionCategory;
import BudgetController.ViewModels.FinancialTransactionViewModel;
import BudgetController.ViewModels.PeriodicalTransactionViewModel;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Date;
import java.util.ResourceBundle;

public class PerformPeriodicalTransactionController extends Controller {
    private PeriodicalTransactionViewModel viewModel = new PeriodicalTransactionViewModel();
    private FinancialTransactionViewModel financialTransactionViewModel = new FinancialTransactionViewModel();

    private Validator<String> amountValidator = new Validator<>();
    private Validator<String> descriptionValidator = new Validator<>();
    private Validator<LocalDate> dateValidator = new Validator<>();

    @FXML
    private Label typeLabel;
    @FXML
    private Label categoryLabel;
    @FXML
    private TextField descriptionTextField;
    @FXML
    private TextField amountTextField;
    @FXML
    private Label amountValidationLabel;
    @FXML
    private Label descriptionValidationLabel;
    @FXML
    private Label dateValidationLabel;
    @FXML
    private DatePicker datePicker;


    @FXML
    private ResourceBundle resources;

    private PeriodicalTransaction periodicalTransaction;

    public PeriodicalTransaction getPeriodicalTransaction() {
        return periodicalTransaction;
    }
    public void setPeriodicalTransaction(PeriodicalTransaction periodicalTransaction) {
        this.periodicalTransaction = periodicalTransaction;
        fillFields();
        initValidators();
    }

    private void initValidators()
    {
        //region amountValidator
        amountValidator.getValidationRules().add(
                new CannotBeEmptyStringValidationRule(resources.getString("fieldRequiredValidationMessage")),
                0);
        amountValidator.getValidationRules().add(
                new IsMoneyValueValidationRule(resources.getString("hasToBeMoneyValueValidationMessage")),
                1);
        amountValidator.getValidationRules().add(
                new IsPositiveMoneyValueValidationRule(resources.getString("hasToBePositiveValidationMessage")),
                2);

        amountValidator.validatedValueProperty().bind(amountTextField.textProperty());

        amountValidationLabel.managedProperty().bind(amountValidator.validProperty().not());
        amountValidationLabel.textProperty().bind(amountValidator.validationMessageProperty());
        //endregion

        //region descriptionValidator
        descriptionValidator.getValidationRules().add(
                new CannotBeEmptyStringValidationRule(resources.getString("fieldRequiredValidationMessage")),
                0);

        descriptionValidator.validatedValueProperty().bind(descriptionTextField.textProperty());

        descriptionValidationLabel.managedProperty().bind(descriptionValidator.validProperty().not());
        descriptionValidationLabel.textProperty().bind(descriptionValidator.validationMessageProperty());
        //endregion

        //region dateValidator
        dateValidator.getValidationRules().add(
                new CannotBeNullValidationRule<>(resources.getString("dateHaveNotBeenSelected")),0);
        dateValidator.validatedValueProperty().bindBidirectional(datePicker.valueProperty());
        dateValidationLabel.managedProperty().bind(dateValidator.validProperty().not());
        dateValidationLabel.textProperty().bind(dateValidator.validationMessageProperty());
        //endregion
    }
    private void fillFields()
    {
        typeLabel.setText(
                convertIsIncomeWrapperField
                        (
                                periodicalTransaction.getCategory().isIncome()
                        )
        );

        categoryLabel.setText(periodicalTransaction.getCategory().getName());

        descriptionTextField.setText(periodicalTransaction.getDescription());
        amountTextField.setText(periodicalTransaction.getDefaultValue().toString());
        datePicker.setValue(
                DateConverter.FromUtilToLocal(periodicalTransaction.getDate())
        );
    }



    private String convertIsIncomeWrapperField(Boolean isIncome)
    {
        return isIncome ? resources.getString("income") : resources.getString("outcome");
    }

    @FXML
    private void perform()
    {
        Boolean amountValid = amountValidator.isValid();
        Boolean descriptionValid = descriptionValidator.isValid();
        Boolean dateValid = dateValidator.isValid();

        if(amountValid && descriptionValid && dateValid)
        {
            addTransactionToHistory();
            updatePeriodicalTransaction();
            navigationController.changeView("/BudgetController/Views/BrowsePeriodicalTransactionsView.fxml");
        }
    }

    private void addTransactionToHistory()
    {
        Date date = java.sql.Date.valueOf(datePicker.getValue());
        String description = descriptionTextField.getText();
        TransactionCategory category = periodicalTransaction.getCategory();
        BigDecimal amount = StringToMoneyConverter.convert(amountTextField.getText());
        amount = category.isIncome() ? amount : amount.negate();

        FinancialTransaction transaction = new FinancialTransaction(amount, date, category, description);
        financialTransactionViewModel.addTransaction(transaction);
    }

    private void updatePeriodicalTransaction()
    {
        LocalDate currentDate = DateConverter.FromUtilToLocal(periodicalTransaction.getDate());
        LocalDate nextDate;

        if(currentDate.getMonthValue() == 12)
        {
            nextDate = currentDate.withMonth(1).withYear(currentDate.getYear() + 1);
        }
        else
        {
            nextDate = currentDate.withMonth(currentDate.getMonthValue() + 1);
        }

        periodicalTransaction.setDate(
                DateConverter.FromLocalToUtil(nextDate)
        );

        viewModel.update(periodicalTransaction);
    }
}
