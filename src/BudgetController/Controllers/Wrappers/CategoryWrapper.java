package BudgetController.Controllers.Wrappers;

import BudgetController.Models.TransactionCategory;
import javafx.beans.property.SimpleStringProperty;

import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;


public class CategoryWrapper {
    private SimpleStringProperty name = new SimpleStringProperty();
    private SimpleStringProperty isIncome = new SimpleStringProperty();
    private int id;
    private TransactionCategory model;

    public int getId() { return id; }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public String getIsIncome() {
        return isIncome.get();
    }

    public SimpleStringProperty isIncomeProperty() {
        return isIncome;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public void setIsIncome(String isIncome) {
        this.isIncome.set(isIncome);
    }

    public CategoryWrapper(TransactionCategory c, ResourceBundle resourceBundle) {
        id=c.getId();
        name.set(c.getName());
        String converted = c.isIncome() ? resourceBundle.getString("income") : resourceBundle.getString("outcome");
        setIsIncome(converted);
        model = c;
    }

    public TransactionCategory getModel() {
        return model;
    }

    @Override
    public String toString()
    {
        return getName();
    }

    public static CategoryWrapper wrapCategory(TransactionCategory model, ResourceBundle resourceBundle)
    {
        return new CategoryWrapper(model, resourceBundle);
    }

    public static List<CategoryWrapper> wrapCategories(List<TransactionCategory> models, ResourceBundle resourceBundle)
    {
        return models.stream()
                .map(o -> new CategoryWrapper(o, resourceBundle)).collect(Collectors.toList());
    }
}
