package BudgetController.Controllers.Wrappers;

import BudgetController.Helpers.DateConverter;
import BudgetController.Models.PeriodicalTransaction;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class PeriodicalTransactionWrapper {
    private PeriodicalTransaction model;
    private SimpleStringProperty categoryName = new SimpleStringProperty();
    private SimpleStringProperty description = new SimpleStringProperty();
    private SimpleStringProperty active = new SimpleStringProperty();
    private SimpleStringProperty date = new SimpleStringProperty();
    private SimpleBooleanProperty realizationDatePast = new SimpleBooleanProperty();

    public PeriodicalTransactionWrapper(PeriodicalTransaction model, ResourceBundle resourceBundle) {
        this.model = model;
        setCategoryName(model.getCategory().getName());
        setDescription(model.getDescription());
        setActive(model.isActive() ? resourceBundle.getString("yes") : resourceBundle.getString("no"));
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        setDate(dateFormat.format(model.getDate()));
    }

    //region Getters and Setters

    public PeriodicalTransaction getModel() {
        return model;
    }

    public String getCategoryName() {
        return categoryName.get();
    }

    public SimpleStringProperty categoryNameProperty() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName.set(categoryName);
    }

    public String getDescription() {
        return description.get();
    }

    public SimpleStringProperty descriptionProperty() {
        return description;
    }

    public void setDescription(String description) {
        this.description.set(description);
    }

    public String getDate() {
        return date.get();
    }

    public SimpleStringProperty dateProperty() {
        return date;
    }

    public void setDate(String date) {
        this.date.set(date);
        this.realizationDatePast.set(model.getDate().before(DateConverter.FromLocalToUtil(LocalDate.now())));
    }

    public boolean isRealizationDatePast() {
        return realizationDatePastProperty().get();
    }

    public SimpleBooleanProperty realizationDatePastProperty() {
        return realizationDatePast;
    }

    public void setRealizationDatePast(boolean realizationDatePast) {
        this.realizationDatePast.set(realizationDatePast);
    }

    public String getActive() {
        return active.get();
    }

    public SimpleStringProperty activeProperty() {
        return active;
    }

    public void setActive(String active) {
        this.active.set(active);
    }

    //endregion

    public static PeriodicalTransactionWrapper wrapPeriodicalTransaction(PeriodicalTransaction model, ResourceBundle resourceBundle)
    {
        return new PeriodicalTransactionWrapper(model, resourceBundle);
    }

    public static List<PeriodicalTransactionWrapper> wrapPeriodicalTransactions(List<PeriodicalTransaction> models, ResourceBundle resourceBundle)
    {
        return models.stream()
                .map(o -> new PeriodicalTransactionWrapper(o, resourceBundle)).collect(Collectors.toList());
    }
}
