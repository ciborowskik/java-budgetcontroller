package BudgetController.Controllers.Wrappers;

import BudgetController.Models.FreezedIncome;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

public class FreezedIncomeWrapper {
    private int id;
    private boolean isActive;
    private StringProperty amount = new SimpleStringProperty();
    private StringProperty startDate = new SimpleStringProperty();
    private StringProperty endDate = new SimpleStringProperty();
    private StringProperty description = new SimpleStringProperty();
    private StringProperty categoryName = new SimpleStringProperty();
    private FreezedIncome model;

    public FreezedIncomeWrapper(FreezedIncome f) {
        id = f.getId();
        isActive = f.getEndDate() ==  null;
        amount.set(f.getAmount().toString());
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        startDate.set(dateFormat.format(f.getStartDate()));
        endDate.set(f.getEndDate() ==  null ? "-" : dateFormat.format(f.getEndDate()));
        description.set(f.getDescription());
        categoryName.set(f.getCategory().getName());
        model = f;
    }

    public int getId() {
        return id;
    }

    public boolean isActive() {
        return isActive;
    }

    public String getAmount() {
        return amount.get();
    }

    public StringProperty amountProperty() {
        return amount;
    }

    public String getStartDate() {
        return startDate.get();
    }

    public StringProperty startDateProperty() {
        return startDate;
    }

    public String getDescription() {
        return description.get();
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    public String getCategoryName() {
        return categoryName.get();
    }

    public StringProperty categoryNameProperty() {
        return categoryName;
    }

    public String getEndDate() {
        return endDate.get();
    }

    public StringProperty endDateProperty() {
        return endDate;
    }

    public FreezedIncome getModel() {
        return model;
    }

    public static FreezedIncomeWrapper wrapFreezedIncome(FreezedIncome model)
    {
        return new FreezedIncomeWrapper(model);
    }

    public static List<FreezedIncomeWrapper> wrapFreezedIncomes(List<FreezedIncome> models)
    {
        return models.stream()
                .map(o -> new FreezedIncomeWrapper(o)).collect(Collectors.toList());
    }
}
