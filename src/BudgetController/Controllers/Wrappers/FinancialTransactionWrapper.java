package BudgetController.Controllers.Wrappers;

import BudgetController.Models.FinancialTransaction;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.stream.Collectors;

public class FinancialTransactionWrapper {
    private int id;
    private StringProperty amount = new SimpleStringProperty();
    private StringProperty date = new SimpleStringProperty();
    private StringProperty description = new SimpleStringProperty();
    private StringProperty categoryName = new SimpleStringProperty();
    private FinancialTransaction model;

    public FinancialTransactionWrapper(FinancialTransaction f) {
        id = f.getId();
        amount.set(f.getAmount().toString());
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        date.set(dateFormat.format(f.getDate()));
        description.set(f.getDescription());
        categoryName.set(f.getCategory().getName());
        model = f;
    }

    public int getId() {
        return id;
    }

    public String getAmount() {
        return amount.get();
    }

    public StringProperty amountProperty() {
        return amount;
    }

    public String getDate() {
        return date.get();
    }

    public StringProperty dateProperty() {
        return date;
    }

    public String getDescription() {
        return description.get();
    }

    public StringProperty descriptionProperty() {
        return description;
    }

    public String getCategoryName() {
        return categoryName.get();
    }

    public StringProperty categoryNameProperty() {
        return categoryName;
    }

    public FinancialTransaction getModel() {
        return model;
    }

    public static FinancialTransactionWrapper wrapFinancialTransaction(FinancialTransaction model)
    {
        return new FinancialTransactionWrapper(model);
    }

    public static List<FinancialTransactionWrapper> wrapFinancialTransactions(List<FinancialTransaction> models)
    {
        return models.stream()
                .map(o -> new FinancialTransactionWrapper(o)).collect(Collectors.toList());
    }
}
