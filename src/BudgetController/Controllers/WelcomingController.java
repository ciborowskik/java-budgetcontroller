package BudgetController.Controllers;

import BudgetController.Main;
import BudgetController.ViewModels.CategoryViewModel;
import BudgetController.ViewModels.FinancialTransactionViewModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.chart.PieChart;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.util.Pair;

import java.math.BigDecimal;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.*;


public class WelcomingController extends Controller implements Initializable {
    private FinancialTransactionViewModel viewModel = new FinancialTransactionViewModel();

    ResourceBundle bundle;

    @FXML
    private ImageView image;
    @FXML
    private Label currentState;
    @FXML
    private Label thisMonth;
    @FXML
    private PieChart chart;


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        bundle = resources;
        image.setImage(new Image(Main.class.getResource("icon.png").toString()));

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_MONTH, 1);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        Date firstDayOfMonth = cal.getTime();

        BigDecimal state = viewModel.allTransactionsSum();
        BigDecimal thisMonthIncome = viewModel
                .getFilteredTransactions(firstDayOfMonth, null, null, null, null, true, null)
                .stream().map(f -> f.getAmount()).reduce(BigDecimal.ZERO, BigDecimal::add);
        BigDecimal thisMonthOutcome = viewModel
                .getFilteredTransactions(firstDayOfMonth, null, null, null, null, false, null)
                .stream().map(f -> f.getAmount()).reduce(BigDecimal.ZERO, BigDecimal::add).negate();

        DecimalFormat df = new DecimalFormat("#,###.00 zł");

        currentState.setText(MessageFormat.format(bundle.getString("budgetState"), df.format(state)));
        thisMonth.setText(MessageFormat.format(bundle.getString("thisMonthFinances"), df.format(thisMonthIncome), df.format(thisMonthOutcome)));

        initializeChart(7, 0.03);
    }

    private void initializeChart(int maxSlicesCount, double minRatio){

        List<Pair<String, Double>> outcomesForCategories = viewModel.outcomesForCategories();

        Double total = outcomesForCategories.stream().mapToDouble(p -> p.getValue()).sum();
        Double others = 0.0;

        ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();

        int slicesCount = 0;
        for (int i = 0; i < outcomesForCategories.size(); i++) {
            if(slicesCount >= maxSlicesCount  || outcomesForCategories.get(i).getValue() / total < minRatio)
                others += outcomesForCategories.get(i).getValue();
            else {
                pieChartData.add(new PieChart.Data(outcomesForCategories.get(i).getKey(), outcomesForCategories.get(i).getValue()));
                slicesCount++;
            }
        }
        pieChartData.add(new PieChart.Data("Others", others));

        chart.setData(pieChartData);
    }
}
