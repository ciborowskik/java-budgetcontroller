package BudgetController.Controllers;

import BudgetController.Controllers.Validation.*;
import BudgetController.Controllers.Wrappers.CategoryWrapper;
import BudgetController.Exceptions.ItemAlreadyExistsException;
import BudgetController.Models.PeriodicalTransaction;
import BudgetController.Models.TransactionCategory;
import BudgetController.ViewModels.CategoryViewModel;
import BudgetController.ViewModels.PeriodicalTransactionViewModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

public class AddPeriodicalTransactionController extends Controller {
    private PeriodicalTransactionViewModel viewModel = new PeriodicalTransactionViewModel();
    private CategoryViewModel categoryViewModel = new CategoryViewModel();

    @FXML
    private ResourceBundle resources;

    @FXML
    private ComboBox categoryComboBox;

    @FXML
    private RadioButton incomeRadioButton;
    @FXML
    private RadioButton outcomeRadioButton;
    @FXML
    private ToggleGroup isIncomeGroup;

    @FXML
    private TextField descriptionTextField;
    @FXML
    private TextField defaultValueTextField;

    @FXML
    private Label descriptionValidationLabel;
    @FXML
    private Label defaultValueValidationLabel;
    @FXML
    private Label generalErrorsLabel;


    @FXML
    private ComboBox dayComboBox;

    private Validator<String> descriptionValidator = new Validator<>();
    private Validator<String> defaultValueValidator = new Validator<>();


    @FXML
    public void initialize() {
        isIncomeGroup.selectedToggleProperty().addListener(
            (obj, oldToggle, newToggle) -> updateCategoryComboBox()
        );
        outcomeRadioButton.setSelected(true);

        fillDayComboBox();
        dayComboBox.getSelectionModel().select(0);
        
        initValidators();
    }

    private void updateCategoryComboBox()
    {
        Boolean isIncome = incomeRadioButton.isSelected();
        List<TransactionCategory> categories = categoryViewModel.getByType(isIncome);
        List<CategoryWrapper> wrappers = CategoryWrapper.wrapCategories(categories, resources);
        ObservableList<CategoryWrapper> observableList =  FXCollections.observableArrayList(wrappers);
        categoryComboBox.setItems(FXCollections.observableArrayList(observableList));
        categoryComboBox.getSelectionModel().select(0);

    }

    private void fillDayComboBox()
    {
        List<Integer> days = new ArrayList<>();
        for(int i = 1; i <= 28; i++)
        {
            days.add(i);
        }
        ObservableList<Integer> observableList = FXCollections.observableArrayList(days);
        dayComboBox.setItems(observableList);
    }
    private void initValidators()
    {

        //region Description
        descriptionValidator.getValidationRules().add(
                new CannotBeEmptyStringValidationRule(resources.getString("fieldRequiredValidationMessage")),
                0);

        descriptionValidator.validatedValueProperty().bind(descriptionTextField.textProperty());

        descriptionValidationLabel.managedProperty().bind(descriptionValidator.validProperty().not());
        descriptionValidationLabel.textProperty().bind(descriptionValidator.validationMessageProperty());
        //endregion

        //region Default Value
        defaultValueValidator.getValidationRules().add(
                new CannotBeEmptyStringValidationRule(resources.getString("fieldRequiredValidationMessage")),
                0);
        defaultValueValidator.getValidationRules().add(
                new IsMoneyValueValidationRule(resources.getString("hasToBeMoneyValueValidationMessage")),
                1);
        defaultValueValidator.getValidationRules().add(
                new IsPositiveMoneyValueValidationRule(resources.getString("hasToBePositiveValidationMessage")),
                2);

        defaultValueValidator.validatedValueProperty().bind(defaultValueTextField.textProperty());

        defaultValueValidationLabel.managedProperty().bind(defaultValueValidator.validProperty().not());
        defaultValueValidationLabel.textProperty().bind(defaultValueValidator.validationMessageProperty());
        //endregion
    }

    @FXML
    private void addPeriodicalTransaction()
    {
        generalErrorsLabel.setText(null);
        generalErrorsLabel.setManaged(false);

        Boolean defaultValueValid = defaultValueValidator.validate();
        Boolean descriptionValid = descriptionValidator.validate();
        if(defaultValueValid && descriptionValid)
        {
            try{
            viewModel.add(createPeriodicalTransaction());
            }
            catch (ItemAlreadyExistsException e)
            {
                generalErrorsLabel.setText(resources.getString("thatTransactionAlreadyExists"));
                generalErrorsLabel.setManaged(true);
                return;
            }
            navigationController.changeView("/BudgetController/Views/BrowsePeriodicalTransactionsView.fxml");
        }
    }

    private PeriodicalTransaction createPeriodicalTransaction()
    {

        return new PeriodicalTransaction(
            getSelectedCategory(),
            descriptionTextField.getText(),
            StringToMoneyConverter.convert(defaultValueTextField.getText()),
            createPaymentDate((int)dayComboBox.getSelectionModel().getSelectedItem()),
    true
        );
    }

    private TransactionCategory getSelectedCategory()
    {
        CategoryWrapper selectedWrapper = (CategoryWrapper)categoryComboBox.getSelectionModel().getSelectedItem();
        return categoryViewModel.getCategoryById(selectedWrapper.getId());
    }

    private Date createPaymentDate(int dayInMonth)
    {
        LocalDate date = LocalDate.now().withDayOfMonth(dayInMonth);
        if(dayInMonth < LocalDate.now().getDayOfMonth())
        {
            if(date.getMonthValue() == 12)
            {
                date = date.withMonth(1).withYear(date.getYear() + 1);
            }
            else
            {
                date = date.withMonth(date.getMonthValue() + 1);
            }
        }

        return java.sql.Date.valueOf(date);
    }

}
