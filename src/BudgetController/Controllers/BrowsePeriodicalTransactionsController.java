package BudgetController.Controllers;

import BudgetController.Controllers.Wrappers.PeriodicalTransactionWrapper;
import BudgetController.ViewModels.PeriodicalTransactionViewModel;
import javafx.beans.binding.Bindings;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.Callback;

import java.util.List;
import java.util.ResourceBundle;

public class BrowsePeriodicalTransactionsController extends Controller {
    private PeriodicalTransactionViewModel viewModel = new PeriodicalTransactionViewModel();
    private PeriodicalTransactionWrapper selectedWrapper;

    @FXML
    private ResourceBundle resources;

    @FXML
    private TableView tableView;
    @FXML
    private CheckBox showInactiveCheckBox;
    @FXML
    private Button deleteButton;
    @FXML
    private Button changeActiveButton;
    @FXML
    private Button realizeButton;
    @FXML
    private TableColumn isActiveColumn;

    @FXML
    public void initialize() {
        showInactiveCheckBox.selectedProperty().addListener(
                (obj, oldValue, newValue) ->
                {
                    if(newValue == true)
                        refreshTableAndSortByActive();
                    else
                        refreshTableAndSortByDate();
                }
        );
        addTableSelectionChangeListener();

        refreshTableAndSortByDate();

        addStyleToTransactionsWithPastDates();

        //show is active column only when both active and inactive transactions are shown
        isActiveColumn.visibleProperty().bind(showInactiveCheckBox.selectedProperty());
    }

    private void addStyleToTransactionsWithPastDates()
    {
        tableView.setRowFactory(new Callback<TableView<PeriodicalTransactionWrapper>, TableRow<PeriodicalTransactionWrapper>>() {
            @Override
            public TableRow<PeriodicalTransactionWrapper> call(TableView<PeriodicalTransactionWrapper> param) {
                final TableRow<PeriodicalTransactionWrapper> row = new TableRow<PeriodicalTransactionWrapper>() {
                    @Override
                    protected void updateItem(PeriodicalTransactionWrapper row, boolean empty) {
                        super.updateItem(row, empty);
                        if (!empty)
                            styleProperty().bind(Bindings.when(row.realizationDatePastProperty())
                                    .then("-fx-font-weight: bold; -fx-font-size: 16;")
                                    .otherwise(""));
                    }
                };
                return row;
            }
        });
    }

    private void updateTable(List<PeriodicalTransactionWrapper> wrappers)
    {
        if(!tableView.getItems().isEmpty()){
            tableView.getItems().clear();
        }
        ObservableList<PeriodicalTransactionWrapper> observableList = tableView.getItems();
        observableList.addAll(wrappers);
    }

    @FXML
    private void refreshTable()
    {
        List<PeriodicalTransactionWrapper> categories = null;
        if(showInactiveCheckBox.isSelected())
        {
            categories = PeriodicalTransactionWrapper.wrapPeriodicalTransactions(viewModel.getAll(), resources);

        }
        else {
            categories = PeriodicalTransactionWrapper.wrapPeriodicalTransactions(viewModel.getByActive(true), resources);
        }
        updateTable(categories);
    }

    private void refreshTableAndSortByDate()
    {
        refreshTable();
        ObservableList<PeriodicalTransactionWrapper> observableList = tableView.getItems();
        observableList.sort((c1, c2) -> c1.getModel().getDate().compareTo(c2.getModel().getDate()));
    }

    private void refreshTableAndSortByActive()
    {
        refreshTable();
        ObservableList<PeriodicalTransactionWrapper> observableList = tableView.getItems();
        observableList.sort((c1, c2) ->
        {
            if(c1.getModel().isActive() == c2.getModel().isActive())
             return 0;
            if(c1.getModel().isActive() == true)
                return 1;
            else
                return -1;
        });
    }


    private void addTableSelectionChangeListener()
    {
        deleteButton.setDisable(true);
        changeActiveButton.setDisable(true);
        realizeButton.setDisable(true);

        tableView.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
            selectedWrapper = (PeriodicalTransactionWrapper) newSelection;

            if (newSelection == null) {
                deleteButton.setDisable(true);
                changeActiveButton.setDisable(true);
                realizeButton.setDisable(true);
            }
            else
            {
                deleteButton.setDisable(false);
                changeActiveButton.setDisable(false);
                if(selectedWrapper.getModel().isActive())
                {
                    realizeButton.setDisable(false);
                }
                else
                {
                    realizeButton.setDisable(true);
                }
            }

        });
    }

    @FXML
    private void changeActive()
    {
        selectedWrapper.getModel().setActive(!selectedWrapper.getModel().isActive());
        viewModel.update(selectedWrapper.getModel());
        refreshTable();
    }

    @FXML
    private void delete()
    {
        viewModel.delete(selectedWrapper.getModel().getId());
        refreshTable();
    }

    @FXML
    private void realize()
    {
        PerformPeriodicalTransactionController controller
                = (PerformPeriodicalTransactionController)navigationController.changeView(
                        "/BudgetController/Views/PerformPeriodicalTransactionView.fxml");
        controller.setPeriodicalTransaction(selectedWrapper.getModel());
    }
}
