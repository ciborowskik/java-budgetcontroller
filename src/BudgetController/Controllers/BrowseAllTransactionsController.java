package BudgetController.Controllers;

import BudgetController.Controllers.Validation.IsEmptyOrMoneyValueValidationRule;
import BudgetController.Controllers.Validation.StringToMoneyConverter;
import BudgetController.Controllers.Validation.Validator;
import BudgetController.Controllers.Wrappers.CategoryWrapper;
import BudgetController.Controllers.Wrappers.FinancialTransactionWrapper;
import BudgetController.ViewModels.CategoryViewModel;
import BudgetController.ViewModels.FinancialTransactionViewModel;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.math.BigDecimal;
import java.net.URL;
import java.util.Date;
import java.util.ResourceBundle;

public class BrowseAllTransactionsController extends Controller implements Initializable {

    private Validator<String> amountFromValidator = new Validator<>();
    private Validator<String> amountToValidator = new Validator<>();
    private FinancialTransactionViewModel financialTransactionViewModel = new FinancialTransactionViewModel();
    private CategoryViewModel categoryViewModel = new CategoryViewModel();
    private ObservableList<FinancialTransactionWrapper> tableViewItems = FXCollections.observableArrayList();
    private ObservableList<CategoryWrapper> comboBoxItems = FXCollections.observableArrayList();

    private ResourceBundle resourceBundle;

    @FXML
    private TableView<FinancialTransactionWrapper> tableView;
    @FXML
    private DatePicker dateFrom;
    @FXML
    private DatePicker dateTo;
    @FXML
    private TextField amountFrom;
    @FXML
    private Label amountFromValidationLabel;
    @FXML
    private TextField amountTo;
    @FXML
    private Label amountToValidationLabel;
    @FXML
    private CheckBox income;
    @FXML
    private CheckBox outcome;
    @FXML
    private ChoiceBox<CategoryWrapper> comboBox;
    @FXML
    private TextField searchString;
    @FXML
    private Button deleteButton;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        resourceBundle = resources;

        amountFromValidator.getValidationRules().add(
                new IsEmptyOrMoneyValueValidationRule(resources.getString("incorrectAmount")), 1);
        amountFromValidator.validatedValueProperty().bind(amountFrom.textProperty());
        amountFromValidationLabel.managedProperty().bind(amountFromValidator.validProperty().not());
        amountFromValidationLabel.textProperty().bind(amountFromValidator.validationMessageProperty());

        amountToValidator.getValidationRules().add(
                new IsEmptyOrMoneyValueValidationRule(resources.getString("incorrectAmount")), 1);
        amountToValidator.validatedValueProperty().bind(amountTo.textProperty());
        amountToValidationLabel.managedProperty().bind(amountToValidator.validProperty().not());
        amountToValidationLabel.textProperty().bind(amountToValidator.validationMessageProperty());

        tableView.setItems(tableViewItems);
        comboBox.setItems(comboBoxItems);

        deleteButton.disableProperty().bind(tableView.getSelectionModel().selectedItemProperty().isNull());

        updateTableView();
        updateComboBox();
    }

    @FXML
    private void updateTableView() {

        Date date_from = dateFrom.getValue() == null ? null : java.sql.Date.valueOf(dateFrom.getValue());
        Date date_to = dateTo.getValue() == null ? null : java.sql.Date.valueOf(dateTo.getValue());
        Boolean type = !income.isSelected() && !outcome.isSelected() ? null : income.isSelected();
        Integer category_id = comboBox.getValue() == null ? null : comboBox.getValue().getId();
        String search_string = searchString.getText() == null || searchString.getText().isEmpty() ? null : searchString.getText();

        BigDecimal amount_from = null;
        boolean v1 = amountFromValidator.isValid();
        if (v1) {
            amount_from = amountFrom.getText().equals("") ? null : StringToMoneyConverter.convert(amountFrom.getText());
        }

        BigDecimal amount_to = null;
        boolean v2 = amountToValidator.isValid();
        if (v2) {
            amount_to = amountTo.getText().equals("") ? null : StringToMoneyConverter.convert(amountTo.getText());
        }

        if (v1 && v2) {
            tableViewItems.setAll(FinancialTransactionWrapper.wrapFinancialTransactions(financialTransactionViewModel.getFilteredTransactions(date_from, date_to, amount_from, amount_to, search_string, type, category_id)));
            tableView.refresh();
        }
    }

    @FXML
    private void deleteTransaction(){
        financialTransactionViewModel.deleteTransactionById(tableView.getSelectionModel().getSelectedItem().getId());
        tableViewItems.remove(tableView.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void incomeSelectionChanged(){
        if(income.isSelected())
            outcome.setSelected(false);
        updateComboBox();
    }

    @FXML
    private void outcomeSelectionChanged(){
        if(outcome.isSelected())
            income.setSelected(false);
        updateComboBox();
    }

    @FXML
    private void updateComboBox() {
        if (income.isSelected())
            comboBoxItems.setAll(CategoryWrapper.wrapCategories(categoryViewModel.getByType(true), resourceBundle));
        else if (outcome.isSelected())
            comboBoxItems.setAll(CategoryWrapper.wrapCategories(categoryViewModel.getByType(false), resourceBundle));
        else
            comboBoxItems.setAll(CategoryWrapper.wrapCategories(categoryViewModel.getAll(), resourceBundle));

        comboBoxItems.add(null);

        comboBox.getSelectionModel().clearSelection();
        comboBox.setValue(null);
    }
}
