package BudgetController.Helpers;

import java.util.*;

/**
 * Keeps it's elements in order specified by priorities (from smallest to biggest)
 * @param <T> type of elements which are stored
 */
public class PriorityList<T> implements Iterable<T> {
    private ArrayList<PriorityElement<T>> elements = new ArrayList<>();

    private class PriorityElement<T>{
        private T value;
        private int priority;

        public PriorityElement(T value, int priority) {
            this.value = value;
            this.priority = priority;
        }
    }

    private void sortElements()
    {
        elements.sort( Comparator.comparingInt(e -> e.priority));
    }

    public void add(T element, int priority)
    {
        elements.add(new PriorityElement<>(element, priority));
        sortElements();
    }

    public void clear()
    {
        elements.clear();
    }

    public Boolean contains(T element)
    {
        return elements.stream().anyMatch(pe -> pe.value == element);
    }

    public Boolean isEmpty()
    {
        return elements.isEmpty();
    }

    public int size()
    {
        return elements.size();
    }

    public Boolean remove(T element)
    {
        return elements.removeIf(e -> e.value == element);
    }

    @Override
    public Iterator<T> iterator()
    {
        return new PriorityListIterator(elements);
    }

    private class PriorityListIterator implements Iterator<T>
    {
        private Iterator<PriorityElement<T>> priorityObjectsIterator;

        public PriorityListIterator(List<PriorityElement<T>> priorityList)
        {
            priorityObjectsIterator = priorityList.iterator();
        }

        @Override
        public boolean hasNext() {
            return priorityObjectsIterator.hasNext();
        }

        @Override
        public T next() {
            return priorityObjectsIterator.next().value;
        }
    }

}

