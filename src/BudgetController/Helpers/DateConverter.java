package BudgetController.Helpers;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class DateConverter {
    public static LocalDate FromUtilToLocal(Date date)
    {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }
    public static Date FromLocalToUtil(LocalDate localDate)
    {
        return java.sql.Date.valueOf(localDate);
    }
}
