# Budget Controller

## Filip Czermiński, Krzysztof Ciborowski

Aplikacja Budget Controller powstała jako projekt zaliczeniowy z przedmiotu Zaawansowane Programowanie Obiektowe i Funkcyjne.

### Użyte technologie

Aplikacja została napisana w technologii Java 1.8 (wersja 1.8.0191). Do tworzenia interfejsu graficznego wykorzystano framework JavaFX oraz język znaczników FXML. Aplikacja korzysta z relacyjnej bazy danych SQLite, z którą komunikuje się za pomocą sterownika sqlite-jdbc-3.23.1. Do pracy z danymi użyto mapowania obiektowo-relacyjnego w postaci frameworku ORMLite 5.1.

### Funkcjonalność

Aplikacja pomaga w kontroli i zarządzaniu budżetem domowym. Umożliwia zapisywanie przychodów i wydatków w wygodny sposób. Pozwala na przydzielanie im kategorii, oraz łatwe ich przeglądanie. Oprócz dodawania transakcji jednorazowych możliwe jest konfigurowanie transakcji okresowych oraz inwestycji.